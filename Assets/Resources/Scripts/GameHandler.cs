﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameHandler : MonoBehaviour
{
		public bool canSetCubeInfoManually = false;
		public bool canBuild = true;
		public bool canMoveCube = false;
		public Cube lastCube;
		public List<Cube> cubes = new List<Cube> ();
		public bool canBuildCubeOnStay = false;
		public bool useGlobalGravity = false;
		public List<Vector3> CubePositions;

		public Cube cubeToMove {
				get;
				set;
		}

		public void AddCube (Cube cube)
		{
				cubes.Add (cube);
		}

		public void RemoveCube (Cube cube)
		{
				lastCube.prevCube.line.SetPosition (1, lastCube.prevCube.transform.position);
				Destroy (lastCube.gameObject);
				cubes.RemoveAt (cubes.Count - 1);
				lastCube = cubes [cubes.Count - 1];
		}

		public void Reset ()
		{
				canBuild = false;
				var ce = cubes.GetEnumerator ();
				while (ce.MoveNext()) {
//						cube.Destroy ();
						Destroy (ce.Current.gameObject);
				}
				cubes.Clear ();
				canBuild = true;
		}

		// Use this for initialization
		void Start ()
		{
//				Screen.showCursor = false;
				PlayerPrefs.SetFloat ("BuildCubeAutoInterval", 2500f);
				CubePositions = new List<Vector3> ();
		}


}