﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour, IPause
{
		public Object cubePrefab;
		public Transform CurrentActiveCube  { get; set; }
		public Transform CurrentMainCube { get; set; }
		public Transform CurrentInvisibleCube { get; set; }
		public float unitScale = 100f;
		public Vector2 offsetPosMoveCube = new Vector2 (-125f, 0f);
		public float angleCorrection = 60f;
		public Transform cubeParent;
		public int resWidth = Screen.width;
		public int resHeight = Screen.height;

		protected bool isPaused;

		private LineRenderer lineRenderer;
		private Cube lastCubeHit;
		private CharacterController characterController;
		private GameHandler gh;
		private bool takeHiResShot = false;

		public void OnPauseGame ()
		{
				isPaused = true;
		}
		
		public void OnResumeGame (bool hideCursor)
		{
				isPaused = false;
		}

		void Start ()
		{
				gh = GameObject.FindObjectOfType<GameHandler> ();
				characterController = GetComponent<CharacterController> ();
				lineRenderer = GetComponentInChildren<LineRenderer> ();
				if (isDebugMode) {
						offsetPosMoveCube = new Vector2 (offsetPosMoveCube.x / 4f, offsetPosMoveCube.y / 4f);
				}
		}

		void Awake ()
		{
				currentCamera = Camera.current;
		}
	
		void LateUpdate ()
		{
		
				if (takeHiResShot) {

//						RenderTexture rt = new RenderTexture (resWidth, resHeight, 24);
//						textureCamera.targetTexture = rt;
//						Texture2D screenShot = new Texture2D (resWidth, resHeight, TextureFormat.RGB24, false);
//						textureCamera.Render ();
//						RenderTexture.active = rt;
//						screenShot.ReadPixels (new Rect (0, 0, resWidth, resHeight), 0, 0);
//						textureCamera.targetTexture = null;
//						RenderTexture.active = null; // JC: added to avoid errors
//						Destroy (rt);
//						byte[] bytes = screenShot.EncodeToPNG ();
//						string filename = ScreenShotName (resWidth, resHeight);
//			Application.CaptureScreenshot(filename);
//						Debug.Log (filename);
//						System.IO.File.WriteAllBytes (filename, bytes);
//						Debug.Log (string.Format ("Took screenshot to: {0}", filename));
//						takeHiResShot = false;

			
						string filename = ScreenShotName (resWidth, resHeight);
						Application.CaptureScreenshot (filename);
						Debug.Log ("image taken...");
						takeHiResShot = false;
				}
		}
	
		public static string ScreenShotName (int width, int height)
		{
				return string.Format ("{0}/screenshots/screen_{1}x{2}_{3}.png",
		                     Application.persistentDataPath,
		                     width, height,
		                     System.DateTime.Now.ToString ("yyyy-MM-dd_HH-mm-ss"));
		}

		void ClearCubes ()
		{
				gh.Reset ();
		}

		void OnSave ()
		{
				takeHiResShot = true;
		}

		void OnRemoveCube (Vector2 position)
		{
				var cam = Camera.current;
				if (cam != null) {
						Vector2 offsetPos = new Vector2 (50.5f, 0f);
						var pos = cam.ScreenToWorldPoint (new Vector3 (position.x + offsetPos.x, position.y + offsetPos.y, 10));
						if (gh.lastCube != null) {
								var pos2 = gh.lastCube.transform.position;
								var dist = Vector2.Distance (pos, pos2);
								Debug.Log (dist);
								if (dist <= 0.75f) {
										Debug.Log ("Removed cube " + gh.lastCube.transform.name);
										gh.RemoveCube (gh.lastCube);
								}
						}
				}
		}

		void OnSetSizeStart (Vector2 position)
		{
				var cam = Camera.current;
				if (cam != null) {
						var pos = cam.ScreenToWorldPoint (new Vector3 (position.x + offsetPosMoveCube.x, position.y + offsetPosMoveCube.y, 10));
						distanceDelta = 0f;
						posOnStart = pos.y;
				}
		}

		void OnSetAngleStart (Vector2 position)
		{
				var cam = Camera.current;
				if (cam != null) {
						var pos = cam.ScreenToWorldPoint (new Vector3 (position.x + offsetPosMoveCube.x, position.y + offsetPosMoveCube.y, 10));
						angleDelta = 0f;
						angleOnStart = pos.y;
				}
		}
	
	
		float posOnStart = 0f;
		float angleOnStart = 0f;
		public float setSizeMultiplier = 10f;
		public float setAngleMultiplier = 0.01f;
		public float setSizeMultiplierStationary = 15f;
		public float setAngleMultiplierStationary = 15f;
	
		float angleDelta = 0f;
		float distanceDelta = 0f;

		void OnSetSizeLastDelta (Vector2 position)
		{
				SetSizeText (distanceDelta * setSizeMultiplierStationary);
		}
		
		void OnSetSize (Vector2 position)
		{
				var cam = Camera.current;
				if (cam != null) {
						var pos = cam.ScreenToWorldPoint (new Vector3 (position.x + offsetPosMoveCube.x, position.y + offsetPosMoveCube.y, 10));		
						if (gh.cubeToMove != null) {
								var deltaY = (pos.y - posOnStart) * setSizeMultiplier;
								if (distanceDelta < deltaY) {
										distanceDelta = deltaY;
								}
								SetSizeText (deltaY);
						}
				}
		}

		void OnSetAngleLastDelta (Vector2 position)
		{
				SetAngleText (gh.cubeToMove.angle + angleDelta * setAngleMultiplierStationary);
		}
	
		void OnSetAngle (Vector2 position)
		{
				var cam = Camera.current;
				if (cam != null) {
						var pos = cam.ScreenToWorldPoint (new Vector3 (position.x + offsetPosMoveCube.x, position.y + offsetPosMoveCube.y, 10));
						if (gh.cubeToMove != null) {
								var deltaY = (pos.y - angleOnStart) * setAngleMultiplier;
								if (angleDelta < deltaY) {
										angleDelta = deltaY;
								}
								SetAngleText (gh.cubeToMove.angle + deltaY);
						}
				}
		}

		public string angleUnitType = "°";
		public string distanceUnitType = "cm";
	
		void SetAngleText (float value)
		{
				var newAngle = value;
				if (newAngle >= 0 && newAngle <= 360) {
						gh.cubeToMove.angle = newAngle;
						gh.cubeToMove.textAngle.text = "" + Mathf.Round (gh.cubeToMove.angle) + " " + angleUnitType;
				}
		}
	
		void SetSizeText (float value)
		{
				var newDist = gh.cubeToMove.distance + value;
				if (newDist >= 0) {
						gh.cubeToMove.distance = newDist;
						gh.cubeToMove.textDistance.text = "" + Mathf.Round (gh.cubeToMove.distance) + " " + distanceUnitType;
				}
		}
	
		public bool isDebugMode = false;
		bool isUsingSnapToGrid = false;

		void OnSnapToGrid ()
		{
				isUsingSnapToGrid = isUsingSnapToGrid ^ true;
		}

		string angleTextGUI = "0";
		string distanceTextGUI = "0";
		private TouchScreenKeyboard keyboard = null;

		private Camera currentCamera;

		public GUIStyle guiStyle;

//		void OnGUI ()
//		{
//				angleTextGUI = GUI.TextField (new Rect (355f, 80f, 150f, 40f), angleTextGUI, 25, guiStyle);
//				distanceTextGUI = GUI.TextField (new Rect (355f, 155f, 150f, 40f), distanceTextGUI, 25, guiStyle);
//				if (gh.cubeToMove != null && gh.canSetCubeInfoManually) {
//						var newAngle = gh.cubeToMove.angle;
//						var newDist = gh.cubeToMove.distance;
//						if (float.TryParse (angleTextGUI, out newAngle)) {
////								SetAngleText (newAngle);
//								if (newAngle >= 0 && newAngle <= 360) {
//										gh.cubeToMove.angle = newAngle;
//										gh.cubeToMove.textAngle.text = "" + Mathf.Round (gh.cubeToMove.angle) + " " + angleUnitType;
//								}
//						}
//						if (float.TryParse (distanceTextGUI, out newDist)) {
//								//SetSizeText (newDist);
//								if (newDist >= 0) {
//										gh.cubeToMove.distance = newDist;
//										gh.cubeToMove.textDistance.text = "" + Mathf.Round (gh.cubeToMove.distance) + " " + distanceUnitType;
//								}
//						}
//				}
//		}

		void OnMoveCube (Vector2 position)
		{
				//http://answers.unity3d.com/questions/727472/screentoworldpoint-on-orthographic-cameras-has-dif.html
				var cam = Camera.current;
				if (cam != null) {
						var pos = cam.ScreenToWorldPoint (new Vector3 (position.x + offsetPosMoveCube.x, position.y + offsetPosMoveCube.y, 10));

						if (gh.cubeToMove != null) {
//						var dist = Vector2.Distance (pos, gh.cubeToMove.cubeOwner.transform.position);


								if (isUsingSnapToGrid) {
										gh.cubeToMove.transform.position = new Vector3 (Round (pos.x),
				                                           Round (pos.y),
				                                           Round (pos.z));
								} else {
										gh.cubeToMove.transform.position = pos;
								}

//						gh.cubeToMove.textDistance.text = "" + (dist * unitScale * 100f) / 100f;// pos.x + ", " + pos.y;
//
//						var a = new Vector2 (gh.cubeToMove.cubeOwner.transform.position.x, gh.cubeToMove.cubeOwner.transform.position.y);
//						var b = new Vector2 (pos.x, pos.y);
//						var angle = Mathf.Atan2 (b.y - a.y, b.x - a.x) * Mathf.Rad2Deg + angleCorrection;
//
//						gh.cubeToMove.textAngle.text = "" + angle;
								if (gh.cubeToMove.prevCube != null) {
										gh.cubeToMove.prevCube.line.SetPosition (1, gh.cubeToMove.transform.position);
										if (gh.cubeToMove != gh.lastCube) {
												gh.cubeToMove.line.SetPosition (0, pos);
										}
								} else {
										gh.cubeToMove.line.SetPosition (0, pos);
								}
						}
				}
		}

		public float snapValue = 0.25f;

		float Round (float input)
		{
				var snappedValue = 0f;
		
				snappedValue = snapValue * Mathf.Round ((input / snapValue));
		
				return snappedValue;
		}

		void OnBuildCube (Vector2 position)
		{
				if (!gh.canBuild)
						return;


		
//				var pos = Helper.ConvertPoint (position);
				//http://answers.unity3d.com/questions/727472/screentoworldpoint-on-orthographic-cameras-has-dif.html
				var cam = Camera.current;
				if (cam == null)
						return;

				var pos = cam.ScreenToWorldPoint (new Vector3 (position.x, position.y, 10));

				if (gh.lastCube != null) {
						var pos2 = gh.lastCube.transform.position;
//						Debug.Log (pos + " - " + pos2);
						var dist = Vector2.Distance (pos, pos2);
						if (dist <= 0.25f) {
								gh.canMoveCube = true;
								gh.cubeToMove = gh.lastCube;
								angleTextGUI = "" + Mathf.Round (gh.lastCube.angle);
								distanceTextGUI = "" + Mathf.Round (gh.lastCube.distance);
//								Debug.Log ("overlapping");
								return;
						}
				}

				if (gh.cubes.Count > 0) {
						foreach (var item in gh.cubes) {
								var pos3 = item.transform.position;
								var dist2 = Vector2.Distance (pos, pos3);
								if (dist2 <= 0.25f) {
										gh.canMoveCube = true;
										gh.cubeToMove = item;
										angleTextGUI = "" + Mathf.Round (item.angle);
										distanceTextGUI = "" + Mathf.Round (item.distance);
										return;
								}
						}
				}

//				var pos = Camera.current.ScreenToWorldPoint (position);
//				Debug.Log (pos + " - " + transform.position);

//				if (gh.lastCube != null) {
//						gh.lastCube.line.SetPosition (0, gh.lastCube.transform.position);
//						gh.lastCube.line.SetPosition (1, pos);
//				}
				
				var go = (GameObject)Instantiate (cubePrefab, pos, Quaternion.identity);
				go.transform.parent = cubeParent;
				var cube = go.GetComponent<Cube> ();

				(cube as IDestroyable).IsDestroyable = true;
//				cube.transform.renderer.material.color = new Color (255f, 255f, 255f);// renderer.material.color;
//				cube.transform.parent = transform;
				//			var currentCubeScript = player.CurrentActiveCube.GetComponent<Cube> ();
				//			currentCubeScript.AddCube(cube);
				cube.transform.name = "Cube #" + gh.cubes.Count;
				//						cube.transform.name = "Cube #" + mainCubeScript.CubePositions.Count;
//				var mainCubeScript = CurrentActiveCube.GetComponent<Cube> ();
//				mainCubeScript.AddCube (cube);
//				//						mainCubeScript.CubePositions.Add (cube.transform.position);
//				gh.CubePositions.Add (cube.transform.position);
//				Debug.Log ("Added cube " + cube.transform.name + ". Cube count: " + gh.CubePositions.Count);
				cube.prevCube = gh.lastCube;
				if (gh.lastCube != null) {
						gh.lastCube.nextCube = cube;
				}
				gh.lastCube = cube;
				gh.AddCube (cube);

				gh.cubeToMove = cube;
				gh.canMoveCube = true;
		}
	
		// Update is called once per frame
		void Update ()
		{
//				if (cubeParent.position != moveScreenToPos) {
				cubeParent.position = Vector3.Slerp (new Vector3 (0f, cubeParent.position.y, 0f), new Vector3 (0f, moveScreenToPos.y, 0f), Time.deltaTime * moveScreenSpeed);
//				}
		}

		public float moveScreenSpeed = 1f;
		Vector3 moveScreenToPos;
		Vector2 moveStartPoint = Vector2.zero;
		Vector2 moveDelta = Vector2.zero;

		void OnMoveScreenStart (Vector2 pos)
		{
				moveStartPoint = pos;
		}
	
		void OnMoveScreen (Vector2 pos)
		{
				var cam = Camera.current;
				if (cam != null) {
						var newDelta = pos - moveStartPoint;
						if (Mathf.Abs (newDelta.y) <= 80f) {
								moveDelta += newDelta;
						}
						moveScreenToPos = cam.ScreenToViewportPoint (new Vector3 (cubeParent.position.x, moveDelta.y, cubeParent.position.z));
						Debug.Log ("Delta Y: " + moveDelta.y + ", moveScreenToPos: " + moveScreenToPos);
				}
		}
	
		private void setLineRenderer (Vector3 start, Vector3 end)
		{
				lineRenderer.SetPosition (0, start);
				lineRenderer.SetPosition (1, end);
		}

		void OnTriggerEnter (Collider collider)
		{
//				Debug.Log (collider.gameObject.name);
				if (collider.gameObject.tag == "roof" && characterController.isGrounded) {
						Debug.Log ("Player died");
				}
		}
}