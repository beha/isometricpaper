﻿using UnityEngine;
using System.Collections;

public class TouchControllerNew : MonoBehaviour
{
		public bool isDebugMode = false;
		public JoystickNew moveTouchPad;
		public JoystickNew touchPad;
		public Transform item1;
		public Transform itemClearCubes;
		public Transform itemSave;
		public Transform gameArea;
		public Transform itemNavigation;
		public Transform itemSetLineAngle;
		public Transform itemSetLineSize;
		public Transform itemSnapToGrid;
		public Transform itemSetAngleOrDistanceText;
		public DebugText debugText;
		public DebugText debugText2;

		private GameHandler gh;
		private InputAction action = InputAction.NONE;
//		bool isTouchInitialTouchLeftSide = false;
//		bool isTouchInitialTouchRightSide = false;

		// Use this for initialization
		void Start ()
		{
				gh = GameObject.FindObjectOfType<GameHandler> ();
				debugText.gameObject.SetActive (isDebugMode);
				debugText2.gameObject.SetActive (isDebugMode);
				//				debugText = GameObject.FindObjectOfType<DebugText> ();
		}

		TouchInfo lastTouch = null;
	
//		Vector2 GetTouchPosAdjusted (Touch touch)
//		{
//				var xPos = Mathf.Clamp ((touch.position.x - fingerDownPos.x) / (touchZone.width / 2), -1, 1);
//				var yPos = Mathf.Clamp ((touch.position.y - fingerDownPos.y) / (touchZone.height / 2), -1, 1);
//				var position = new Vector2 (xPos, yPos);
//				return position;
//		}

		public AudioClip audio1;

		// Update is called once per frame
		void Update ()
		{
				var count = Input.touchCount;
				for (int i = 0; i < count; i++) {
						Touch touch = Input.GetTouch (i);

						var pos = touch.position;

						if (touch.phase == TouchPhase.Began) {
								gh.canSetCubeInfoManually = false;
								gh.canMoveCube = false;
								action = InputAction.NONE;
								Debug.Log ("started");
								if (IsItemHit (pos, itemClearCubes)) {
										AudioSource.PlayClipAtPoint (audio1, transform.position);
										gameObject.SendMessage ("ClearCubes", itemClearCubes, SendMessageOptions.DontRequireReceiver);
								} else if (IsItemHit (pos, itemSave)) {
										gameObject.SendMessage ("OnSave", SendMessageOptions.DontRequireReceiver);
										AudioSource.PlayClipAtPoint (audio1, transform.position);
								} else if (IsItemHit (pos, itemSetLineAngle)) {
										gameObject.SendMessage ("OnSetAngleStart", pos, SendMessageOptions.DontRequireReceiver);
										action = InputAction.SET_ANGLE;
								} else if (IsItemHit (pos, itemSetLineSize)) {
										gameObject.SendMessage ("OnSetSizeStart", pos, SendMessageOptions.DontRequireReceiver);
										action = InputAction.SET_SIZE;
								} else if (IsItemHit (pos, itemNavigation)) {
										gameObject.SendMessage ("OnMoveScreenStart", pos, SendMessageOptions.DontRequireReceiver);
										action = InputAction.MOVE_SCREEN;
								} else if (IsItemHit (pos, itemSnapToGrid)) {
										AudioSource.PlayClipAtPoint (audio1, transform.position);
										gameObject.SendMessage ("OnSnapToGrid", SendMessageOptions.DontRequireReceiver);
								} else if (IsItemHit (pos, itemSetAngleOrDistanceText)) {
										action = InputAction.SET_CUBE_ANGLE_OR_DISTANCE;
										gh.canSetCubeInfoManually = true;
										gh.canBuild = false;
								} else {
										if (touch.tapCount == 2) {
//												AudioSource.PlayClipAtPoint (audio1, transform.position);
												gameObject.SendMessage ("OnRemoveCube", pos, SendMessageOptions.DontRequireReceiver);
										} else {
												//										Debug.Log (gh.canMoveCube);
												//										if (!gh.canMoveCube) {
												if (action == InputAction.NONE) {
//														AudioSource.PlayClipAtPoint (audio1, transform.position);
														gameObject.SendMessage ("OnBuildCube", pos, SendMessageOptions.DontRequireReceiver);
												}
												//										}
										}
								}
						} else if (touch.phase == TouchPhase.Canceled) {
								Debug.Log ("ended");
								gh.canMoveCube = false;
								action = InputAction.NONE;
						}
						if (touch.phase == TouchPhase.Moved) {
								Debug.Log ("canMoveCube: " + gh.canMoveCube + ", action: " + action);
								if (gh.canMoveCube) {
										Debug.Log ("moving...");
										gameObject.SendMessage ("OnMoveCube", pos, SendMessageOptions.DontRequireReceiver);
								}
								switch (action) {
								case InputAction.SET_ANGLE:
										gameObject.SendMessage ("OnSetAngle", pos, SendMessageOptions.DontRequireReceiver);
										break;
								case InputAction.SET_SIZE:
										gameObject.SendMessage ("OnSetSize", pos, SendMessageOptions.DontRequireReceiver);
										break;
								case InputAction.MOVE_SCREEN:
										gameObject.SendMessage ("OnMoveScreen", pos, SendMessageOptions.DontRequireReceiver);
										break;
								}
						}
						if (touch.phase == TouchPhase.Stationary) {
								switch (action) {
								case InputAction.SET_ANGLE:
										gameObject.SendMessage ("OnSetAngleLastDelta", pos, SendMessageOptions.DontRequireReceiver);
										break;
								case InputAction.SET_SIZE:
										gameObject.SendMessage ("OnSetSizeLastDelta", pos, SendMessageOptions.DontRequireReceiver);
										break;
								}
						}
						if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
								gh.canMoveCube = false;
								Debug.Log ("ended");
								action = InputAction.NONE;
						}
						if (isDebugMode) {
								debugText2.SetText ("pos: " + pos + ", fid: " + touch.fingerId);
						}
				}

//				foreach (var touchInfo in touchPad.Touches) {
//						var touch = touchInfo.Value;
//
//						if (touch.Touch.fingerId == 0) {
//								// if finger initially is touched on left side then set it to be touch move
//								// if finger initially is touched on right side then set it to be touch rotate
//								// on touch end set touch move and touchrotate to null
////								isTouchInitialTouchLeftSide = touch.Touch.phase == TouchPhase.Began && touch.IsOnLeftSide;
////								isTouchInitialTouchRightSide = touch.Touch.phase == TouchPhase.Began && !touch.IsOnLeftSide;
////								if (isTouchInitialTouchLeftSide) {
////										touchMove = touch;
////										//move
////								}
////								if (isTouchInitialTouchRightSide) {
////										touchRotate = touch;
////								}
////								if (touch.Touch.phase == TouchPhase.Ended) {
////										touchMove = null;
////										touchRotate = null;
////								}
//						}
//			
//						
//						if (touch.Touch.phase == TouchPhase.Began) {
//								gh.canMoveCube = false;
//								action = InputAction.NONE;
//								Debug.Log ("started");
//								lastTouch = touch;
//								if (IsItemHit (touch.Touch.position, itemClearCubes)) {
//										gameObject.SendMessage ("ClearCubes", itemClearCubes, SendMessageOptions.DontRequireReceiver);
//								} else if (IsItemHit (touch.Touch.position, itemSave)) {
//										gameObject.SendMessage ("OnSave", SendMessageOptions.DontRequireReceiver);
//								} else if (IsItemHit (touch.Touch.position, itemSetLineAngle)) {
//										gameObject.SendMessage ("OnSetAngleStart", touch.Touch.position, SendMessageOptions.DontRequireReceiver);
//										action = InputAction.SET_ANGLE;
//								} else if (IsItemHit (touch.Touch.position, itemSetLineSize)) {
//										gameObject.SendMessage ("OnSetSizeStart", touch.Touch.position, SendMessageOptions.DontRequireReceiver);
//										action = InputAction.SET_SIZE;
//								} else if (IsItemHit (touch.Touch.position, itemNavigation)) {
//										gameObject.SendMessage ("OnMoveScreen", SendMessageOptions.DontRequireReceiver);
//								} else {
//										if (touch.Touch.tapCount == 2) {
//												gameObject.SendMessage ("OnRemoveCube", touch.Touch.position, SendMessageOptions.DontRequireReceiver);
//										} else {
////										Debug.Log (gh.canMoveCube);
////										if (!gh.canMoveCube) {
//												gameObject.SendMessage ("OnBuildCube", touch.Touch.position, SendMessageOptions.DontRequireReceiver);
////										}
//										}
//								}
//						} else if (touch.Touch.phase == TouchPhase.Canceled) {
//								Debug.Log ("ended");
//								gh.canMoveCube = false;
//								action = InputAction.NONE;
//								lastTouch = null;
//						}
//						if (lastTouch != null && lastTouch.Touch.phase == TouchPhase.Moved) {
//								Debug.Log ("canMoveCube: " + gh.canMoveCube + ", action: " + action);
//								if (gh.canMoveCube) {
//										Debug.Log ("moving...");
//										gameObject.SendMessage ("OnMoveCube", touch.Touch.position, SendMessageOptions.DontRequireReceiver);
//								}
//								switch (action) {
//								case InputAction.SET_ANGLE:
//										gameObject.SendMessage ("OnSetAngle", touch.Touch.position, SendMessageOptions.DontRequireReceiver);
//										break;
//								case InputAction.SET_SIZE:
//										gameObject.SendMessage ("OnSetSize", touch.Touch.position, SendMessageOptions.DontRequireReceiver);
//										break;
//								}
//						}
//						if (lastTouch != null && lastTouch.Touch.phase == TouchPhase.Ended || touch.Touch.phase == TouchPhase.Canceled) {
//								gh.canMoveCube = false;
//								Debug.Log ("ended");
//								lastTouch = null;
//								action = InputAction.NONE;
//						}
//						if (isDebugMode) {
//								debugText2.SetText ("pos: " + touch.Touch.position + ", fid: " + touch.Touch.fingerId + ", left?: " + touch.IsOnLeftSide);
//						}
//				}
		
				
		}

		bool IsItemHit (Vector2 position, Transform item, float tolerance = 65f)
		{
				bool isHit = false;
				
				var dist = Vector2.Distance (position, new Vector2 (Screen.width * item.position.x, Screen.height * item.position.y));
				
				isHit = dist < tolerance;
				if (isHit) {
						Debug.Log (item.name + " was hit. Distance: " + dist + ", Position: " + item.position);
				}
				return isHit;
		}

		enum InputAction
		{
				NONE,
				SET_ANGLE,
				SET_SIZE,
				MOVE_SCREEN,
				BUILD_CUBE,
				SET_CUBE_ANGLE_OR_DISTANCE
		}
}