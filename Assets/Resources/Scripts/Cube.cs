﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class CubeBase : MonoBehaviour
{

}

public class Cube : CubeBase, IDestroyable
{
		public TextMesh textDistance;
		public TextMesh textAngle;
		public Cube prevCube;
		public Cube nextCube;
		public LineRenderer line;
	
		public float distance = 0f;
		public float angle = 0f;

		public List<Vector3> CubePositions;

		private Color _startColor;

		public void SetColor (Color color)
		{
				//				renderer.material.shader = Shader.Find ("Specular");
				renderer.material.SetColor ("_Color", color);
		}

		public void ResetColor ()
		{
				//				renderer.material.shader = Shader.Find ("Specular");
				renderer.material.SetColor ("_Color", _startColor);
		}

//		public void SetVisible (bool visible)
//		{
//				transform.GetComponent<MeshRenderer> ().enabled = visible;
//		}
	
		public List<Cube> cubes;
	
		public void AddCube (Cube cube)
		{
				cubes.Add (cube);
		}
	
		private void RemoveCube (Cube cube)
		{
//				foreach (var item in cube.cubes) {
//						item.RemoveCube ();
//				}
				cubes.Remove (cube);
		}

		public void RemoveCube ()
		{
				RemoveCube (this);
				Destroy (gameObject);
		}

	#region IDestroyable implementation
	
		public bool IsDestroyable { get; set; }

		public void Destroy ()
		{
				//RemoveCube ();
		}

	#endregion

		void Start ()
		{
//				textDistance.text = "";
//				textAngle.text = "";
				//_startColor = renderer.material.GetColor ("_Color");
		}

		// Update is called once per frame
		void Update ()
		{
//				if (prevCube != null) {
//						line.SetPosition (0, prevCube.transform.position);
//				}
//				if (nextCube != null) {
//						line.SetPosition (1, nextCube.transform.position);
//				}
				if (prevCube != null) {
						prevCube.line.SetPosition (0, prevCube.transform.position);
						prevCube.line.SetPosition (1, transform.position);
				}
		}
	
		public bool CanAdd ()
		{
				bool canAdd = true;
				foreach (var cube in cubes) {
//						Debug.Log (cube.name);
						if (cube != null && cube.transform.position == transform.position) {
								canAdd = false;
								break;
						}
				}
				return canAdd;
		}

		public Cube ()
		{
				cubes = new List<Cube> ();
		}

}